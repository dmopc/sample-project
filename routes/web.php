<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controllers\Products::class, 'index']);
Route::get('/products/{product}/{name}', [Controllers\Products::class, 'show']);

Route::match(['GET', 'POST', 'PUT', 'DELETE'], '/cart', [Controllers\Cart::class, 'cart']);

Route::get('/checkout', [Controllers\Orders::class, 'index']);
Route::post('/checkout', [Controllers\Orders::class, 'store']);
Route::get('/orders', [Controllers\Orders::class, 'show']);
Route::get('/orders/{order_id}', [Controllers\Orders::class, 'edit']);
