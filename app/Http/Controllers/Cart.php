<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class Cart extends Controller
{
    public function cart(Request $request) {
        switch ( strtoupper( $request->method() ) )
        {
            case 'GET':
                $result = $this->showCart();
                break;
            case 'POST':
                $result = $this->add_to_cart($request);
                break;
            case 'PUT':
                $result = $this->update($request);
                break;
            case 'DELETE':
                $result = $this->deleteFromCart($request);
                break;
        }

        return $result;
    }

    protected function showCart()
    {

    }

    protected function add_to_cart($request)
    {
        event(new \App\Events\ProductAddedToCart($request->product_id));
        $cart = session()->get('cart');

        $product = \App\Models\Product::where('id', $request->product_id)->first();

        $cart[] = [
            'product_id' => $request->product_id,
            'quantity' => $request->qty,
            'price' => $product->price
        ];

        session()->put('cart', $cart);

        return redirect('cart');
    }

    protected function update($request)
    {

    }

    protected function deleteFromCart($request)
    {

    }
}
