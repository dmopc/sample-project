<?php

namespace App\Http\Controllers;

use App\Events\ProductWasViewed;
use App\Models\Product;
use Illuminate\Http\Request;

class Products extends Controller
{
    public function index()
    {
        return view('products.index', [
            'products' => Product::all()
        ]);
    }

    public function show(Product $product)
    {
        event(new ProductWasViewed($product));

        return view('products.show', [
            'product' => $product
        ]);
    }
}
