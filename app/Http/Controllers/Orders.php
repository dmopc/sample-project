<?php

namespace App\Http\Controllers;

use App\Lib\Payment\CreditCard;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class Orders extends Controller
{
    public function index()
    {
        return view('checkout');
    }

    public function store(Request $request)
    {
        $cc = new CreditCard;
        $transaction_id = $cc->charge($request->only('credit_card_number', 'credit_card_exp', 'credit_card_cvv'));

        $order = Order::create($request->all());

        $order->transaction_id = $transaction_id;
        $order->save();

        foreach (session()->get('cart') as $cart_prod) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $cart_prod['product_id'],
                'quantity' => $cart_prod['quantity'],
                'price' => $cart_prod['price'],
            ]);
        }

        return redirect('/')->with('message', 'Thank you for your order.');
    }

    public function show()
    {
        $ordersId = request()->get('oID');

        $o = \DB::select("select * from orders where id = $ordersId")[0];

        $order_products = Order::find($ordersId)->products;

        $total_price = 0;
        foreach ($order_products as $order_p)
        {
            $total_price = $total_price + ($order_p->quantity * $order_p->price);
        }

        if ($o) {
            if ($_GET['format'] == 'json') {
                return array(
                    'order' => $o,
                    'products' => $order_products,
                    'total' => $total_price
                );
            } elseif ($_GET['format'] == 'xml') {
                echo 'The XML format has been deprecated.';
                exit;
            } else {
                return view('orders.show', ['products' => $order_products, 'order' => $o, 'total' => $total_price]);
            }
        } else {
            abort(404);
        }
    }

    public function edit($order_id) {
        $o = \DB::select("select * from orders where id = " . $order_id)[0];

        if ($o) {
            // we want to abort if no order was found
        } else {
            abort(404);
        }

        $op = Order::where('id', $o->id)->first()->products;

        for ($i=0;$i<count($op);$i++)
        {
            $product_details = Product::where('id', $op[$i]->id)->first();

            $op[$i]['productDetails'] = $product_details;
        }

        $total_price = 0;
        foreach ($op as $order_p)
        {
            $total_price = $total_price + ($order_p->quantity * $order_p->price);
        }

        // We don't want to allow digital products to be edited
        foreach ($op as $i => $order_p) {
            if ($order_p['productDetails']->is_digital) {
                unset($op[$i]);
            }
        }

        return view('orders.edit',['products'=>$op,'order'=>$o,'total'=>$total_price]);
    }
}
