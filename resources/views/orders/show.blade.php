@extends('layout')

@section('content')
    <div>Order #{{ $order->id }}</div>
    <div>Products</div>
    @foreach ($products as $product)
        <p>{{ $product->name }} x {{ $product->quantity }}</p>
    @endforeach
    <p>Order Total: ${{ number_format($total, 2) }}</p>
@endsection